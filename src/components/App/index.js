import React, { Component } from 'react';

import localStorage from 'localstoragedb';
import { createStorageDefault } from '../../localStorage';

import Table from '../Table';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: ''
    }
  }

  componentWillMount() {
    createStorageDefault();
  }

  onChange = (e) => this.setState({ code: e.target.value })

  render() {
    const store = new localStorage('clientDB', localStorage);
    const { code } = this.state;
    const books = code !== '' ? store.queryAll('books', {query: { code }}) : store.query('books');

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="container">
          <div className="filter">
            <span>Filter by code:</span>
            <input type="text" value={code} placeholder="INPUT CODE" onChange={this.onChange}/>
          </div>
          <div className="wrap-table">
            <Table books={books} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
