import React, { Component } from 'react';
import './style.css';

class Table extends Component {
    renderRow = book => {
        const { ID, code, title, author, year } = book;
        return (
            <tr key={ID}>
                <td>{ID}</td>
                <td>{code}</td>
                <td>{title}</td>
                <td>{author}</td>
                <td>{year}</td>
            </tr>
        );
    }
    
    render() {
        const { books } = this.props;
        const bookItems = books.map(b => this.renderRow(b));

        return (
            <table>
                <thead>
                    {this.renderRow({ ID: 'ID', code: 'Code', title: 'Title', author: 'Author', year: 'Year' })}
                </thead>
                <tbody>
                    {bookItems.length > 0 ? bookItems : 'no record'}
                </tbody>
            </table>
        );
    }
}

export default Table;
